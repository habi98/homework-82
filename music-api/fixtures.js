const mongoose = require('mongoose');
const config = require('./config');

const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;
    const collections = await connection.db.collections();

    for(let collection of collections) {
        await  collection.drop()
    }


    const artists = await Artist.create(
        {
            name: 'Eminem',
            image: 'eminem.jpeg',
            text: 'Ма́ршалл Брюс Мэ́терс III, более известный под сценическим псевдонимом Эмине́м и альтер-эго Слим Шейди — американский рэпер, музыкальный продюсер, композитор и актёр. Помимо сольной карьеры, Маршалл также состоит в группе D12 и хип-хоп-дуэте Bad Meets Evil.'
        },
        {
            name: 'Imagine Dragons',
            image: 'imgd.jpeg',
            text: 'Imagine Dragons — американская инди-группа, образованная в Лас-Вегасе в 2008 году, 20 июля. Стали известны после выпуска дебютного студийного альбома Night Visions в сентябре 2012 года.'
        }
    );

  const album =  await Album.create(
        {
            name: 'Revival',
            artist: artists[0]._id,
            year: '2017',
            image: 'reval.jpg'

        },
        {
            name: 'Evolve',
            artist: artists[1]._id,
            year: '2017',
            image: 'evolve.jpg'

        },
        {
            name: 'Origins',
            artist: artists[1]._id,
            year: '2018',
            image: 'origins.jpg'

        }
    );

  await Track.create(
      {name: 'River', album: album[0]._id, duration: '3:41', trackNumber: 1},
      {name: 'Believe', album: album[0]._id, duration: '5:15', trackNumber: 2},
      {name: 'Heat', album: album[0]._id, duration: '4:10', trackNumber: 3},
      {name: 'Like Home', album: album[0]._id, duration: '4:05', trackNumber: 4},
      {name: 'Believer', album: album[1]._id, duration: '3:24', trackNumber: 1},
      {name: 'Thunder', album: album[1]._id, duration: '3:07', trackNumber: 2},
      {name: 'Yesterday', album: album[1]._id, duration: '3:25', trackNumber: 3},
      {name: 'Natural', album: album[2]._id, duration: '3:09', trackNumber: 1},
      {name: 'Birds', album: album[2]._id, duration: '3:39', trackNumber: 2},
      {name: 'Cool Out', album: album[2]._id, duration: '3:38', trackNumber: 3}
  );

    await connection.close()
};

run().catch(error => {
    console.log('Something went wrong', error)
});