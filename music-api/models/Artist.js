const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ArtistsSchema = new Schema({
   name: {
       type: String, required: true
   },
   image: String,
   text: String
});

const Artist = mongoose.model('Artist', ArtistsSchema);

module.exports = Artist;