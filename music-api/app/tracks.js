const express = require('express');
const Track = require('../models/Track');
const router = express.Router();


router.get('/', (req, res) => {
    let query;

    if (req.query.album) {
        query = {album: req.query.album}
    }
    Track.find(query).sort({trackNumber: 1}).populate('album')
        .then(track => res.send(track))
        .catch(() => res.sendStatus(500))
});


module.exports = router;


