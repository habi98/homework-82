const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const router = express.Router();


router.post('/', async (req, res)=> {

    const token = req.get('Authorization');

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'User not found'})
    }


    const trackHistory =  new TrackHistory(req.body);

    trackHistory.user = user._id;

    trackHistory.datetime = new Date().toISOString();

    trackHistory.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});


module.exports = router;